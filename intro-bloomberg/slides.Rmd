---
title: "Bloomberg en 60 minutes"
author: "<small><i class='fas fa-portrait'></i> info.sdm<span>@</span>fsa.ulaval.ca</small>"
date: "<span style='color:gray'><small><i class='far fa-clock'></i> `r format(Sys.time(), '%Y-%m')`</small></span>"
output:
  revealjs::revealjs_presentation:
    transition: fade
    background_transition: fade
    self_contained: false
    reveal_plugins: ["zoom", "notes", "menu"]
    css: css/styles.css
    theme: black
    center: true
    reveal_options:
      chalkboard:
        theme: whiteboard
        toggleNotesButton: true
      menu:
        numbers: false
      previewLinks: false
      slideNumber: false
---

# Connexion

## Web
<small>Temporairement, instructions [ici](http://sdmapps.ca/latrousse/fr/bloomberg/bba/web/)</small>

1. Compte sur le portail BMC
2. Demande de création de login terminal via le portail BMC
3. Accès au terminal sur `https://bba.bloomberg.net`

<br>

<a href="https://discord.com/invite/9UG3RUr" target="_blank"><i class="fab fa-discord"></i></a>
<small>Questions ? </small>

## Clavier
<small>PAP-3324 (8 terminaux), PAP-3244 (2) et PAP-3219 (1)</small>

![Clavier Bloomberg](./images/keyb.png)

1. <i class="fa fa-windows fa-sm"></i>
2. « bloo ... »
3. `<Entrée>`

<!--
## Compte

1. `<Entrée> | <GO>`
2. `Create a New Login`
-->

# Navigation

## Panneaux

+ Barre d'outils
+ Invite de commande
+ Contenu de la fonction


<br>

<a href="https://data.bloomberglp.com/professional/sites/10/Getting-Started-Guide-for-Students-English.pdf" target="_blank"><i class="fab fa-readme"></i></a>
<small>Guide Bloomberg</small>

## Fonctions et titres

+ Mnémonique :
  + `WEI`
  + `BB CN Equity`
  + `BB US Equity`
+ Type de fonctions :
  + `WEI` : pas besoin de titre
  + `GP`  : besoin de titre
  + `YAS` : besoin de titre (obligation)

<i class="fa fa-keyboard"></i> Recherche par mot clé : « inflation ».

## Titres
<small> Un titre est un instrument financier (actions, obligations, etc.) à analyser avec Bloomberg</small>

+ `AAPL US <Equity>`
+ `037833100 <Equity>`
+ Par mot clé

## Recherche basique
<small>Recherche avec saisie semi-automatique</small>

1. « INFLA ... »
2. <i class="fa fa-caret-up fa-sm"></i> <i class="fa fa-caret-down fa-sm"></i>
3. `<Entrée>`

## Recherche complète

1. « BLACKBERRY EARNINGS »
2. `<Search>`

## Menus
<small>Hiérarchie</small>

1. `<Menu>`
2. `<Menu>`
3. `...`
4. Accueil
5. Equities > Analyze COMPANY NAME Equity > Company Analysis > Financial Analysis > FA
6. `<Equity> <Entrée>`
7. BB CN `<Equity> <Entrée>`

## Fonctions
<small>`WEI, DES, CN, HP, GP, GIP, DVD, ERN, FA, RG, RELS, G` </small>

1. Barre d'outils
2. Champs de saisie
3. Nombre `<Entrée>`
4. Zone cliquable

<i class="fa fa-keyboard"></i> `BPS <Entrée>`<br>
<i class="fa fa-keyboard"></i> `BU  <Entrée>` : cliquer sur  *Access Training Documents*

# Extraction de données

## Complément Excel
<small>Activation/Réparation du complément</small>

1. <i class="fa fa-windows fa-sm"></i> + `D`
2. Double clic sur *Bloomberg API Diagnostics*
3. Clic sur *Repair*
4. Clic sur *Start*

## Assistant de création de chiffrier
<small>*Spreadsheet Builder*</small>

1. Choisir type de données recherchées
2. Choisir le titre
3. Choisir les champs d'extraction
4. Suivre les étapes et cliquer sur *Finish*

<i class="fa fa-keyboard"></i> `FLDS <Entrée>`

## Assistant de création de formules
<small>Function Builder</small>

```{}
=BDH(Titre, Champs, date de départ, date de fin, ...)
```

## Gabarits
<small>Template Library</small>

1. `XLTP XCS <Entrée>`
2. `HELP DAPI <Entrée>`

# Certification et Aide

## Bloomberg Market Concepts

+ Sur le terminal :

```{}
BMC <Entrée>
```

+ En ligne sur le [portail BMC](http://sdmapps.ca/latrousse/fr/bloomberg/bba/web/#compte-bmc)

## Aide

+ Terminal :

```{}
<Help>
<Help> <Help>
```

+ Assistance des Salles des marchés via Discord <a href="https://discord.com/invite/9UG3RUr" target="_blank"><i class="fab fa-discord"></i></a> du lundi au vendredi de 8h à 16h.

